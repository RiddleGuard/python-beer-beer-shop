"""beer_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import main.views
import cart.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^item/(?P<item_id>\d+)', main.views.item, name='Сылки товаров'),
    url(r'^(?P<cat_id>\d+)', main.views.get_category, name='Сылки категорий'),
    url(r'^auth/', include('loginsys.urls')),
    url(r'^cart/', include('cart.urls', namespace='cart')),
    url(r'^order/', include('order.urls', namespace='order')),
    url(r'^$', main.views.home, name='home'),
    #url(r'^sidebar/', include('sidebar.urls')),
    #url(r'^auth/', include('loginsys.urls')),
    #url(r'^', include('article.urls')),
]
