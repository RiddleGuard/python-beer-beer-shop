
#from cart.models import order, all_order 
from main.models import Item,Category
# Create your views here.
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.views.decorators.http import require_POST
from django.contrib import auth
from .cart import Cart
from .forms import CartAddProductForm


# ------------------------------------------------------------------------------------------------

def CartAdd(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Item, id = product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product, quantity=cd['quantity'],
                                  update_quantity=cd['update'])
    if product and (not form.is_valid()):
        cart.add(product=product, quantity=1)
        return redirect('/')  
    return redirect('cart:CartDetail')    
    
    
def CartRemove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Item, id = product_id)
    cart.remove(product)
    return redirect('cart:CartDetail')
    


    
def CartDetail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
                                        initial={
                                            'quantity': item['quantity'],
                                            'update': True
                                        })
    return render(request, 'detail.html', {'cart': cart , 'username': auth.get_user(request).username})
    
# ------------------------------------------------------------------------------------------------   
 