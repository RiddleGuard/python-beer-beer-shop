from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm 
from cart.cart import Cart

def login(request):
    cart = Cart(request)
    args = {}
    args.update(csrf(request))
    args['cart']= cart
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            args['login_error'] = "Пользователь не найден"
            return render_to_response('login.html', args)
    else:
        return render_to_response('login.html', args)
        
def logout(request):
    auth.logout(request)
    return redirect('/')
    
def register(request):
    cart = Cart(request)
    args = {}
    args.update(csrf(request))
    args['cart']= cart
    args['form'] = UserCreationForm(request.POST)
    if request.POST:
        newuser_from = UserCreationForm(request.POST)
        if newuser_from.is_valid():
            newuser_from.save()
            newuser = auth.authenticate(username=newuser_from.cleaned_data['username'], password=newuser_from.cleaned_data['password2'])
            auth.login(request, newuser)
            return redirect('/')
        else:
            args['form'] = newuser_from
    return render_to_response('register.html', args)