from django.db import models
from django.contrib import auth
from datetime import *
# Create your models here.
class Category(models.Model):
    name=models.CharField(default=' ', max_length=255, verbose_name='Название категории')
    visible = models.BooleanField(verbose_name="Отображение")
    class Meta:
        db_table = "category"
        verbose_name="Категория"
        verbose_name_plural="Категории"
    def __str__(self):
        return 'Категория %s' % self.name

class  Item(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название товара")
    price = models.IntegerField(default=0, verbose_name = 'Цена')
    image = models.CharField(max_length=255, verbose_name='Ссылка на картинку')
    text = models.TextField(verbose_name="Описание")
    visible = models.BooleanField(default=True, verbose_name="Отображение")
    date =models.DateField(auto_now=True, verbose_name="Дата публикации")
    category = models.ForeignKey(Category)
    kol = models.IntegerField(default=0, verbose_name = 'Количество на складе')
    class Meta:
        db_table = "goods"
        verbose_name="Товар"
        verbose_name_plural="Товары"
    def __str__(self):
        return 'Товар %s' % self.name
        
class Comments(models.Model):
    class Meta:
        db_table = "comments"
    comments_text = models.TextField(verbose_name = "Текст комментария")
    comments_article = models.ForeignKey(Item)
