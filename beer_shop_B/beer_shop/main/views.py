from django.shortcuts import render,render_to_response
from main.models import Item, Category
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from cart.forms import CartAddProductForm
from django.template.context_processors import csrf
from django.contrib import auth
#from django.core.context_processors import csrf
#from django.views.decorators.csrf import csrf_exempt
from cart.cart import Cart
from datetime import *
import random
import string

# Create your views here.

def home(request):
    cart=Cart(request)
    category =  Category.objects.filter(visible = True)
    goods = Item.objects.filter(visible = True)
    context = {
        'title': 'Hello world',
        'category' : category,
        'goods' : goods,
        'username': auth.get_user(request).username,
        'cart': cart
    }
    return render_to_response('goods.html', context)
    
def item(request, item_id):
    cart=Cart(request)
    try:
        category =  Category.objects.filter(visible = True)
        goods = Item.objects.get(id=item_id)
        cart_product_form = CartAddProductForm()
    except:
        raise Http404('Объект не найден')
    context = {}
    context.update(csrf(request))
    context['goods'] = goods
    context['category'] = category
    context['cart_product_form'] = cart_product_form
    context['username'] = auth.get_user(request).username
    context['cart']= cart
    return render_to_response('item.html', context)
    
def get_category(request, cat_id):
    try:
        cart=Cart(request)
        category_goods =  Category.objects.get(id=cat_id)
        category_menu =  Category.objects.filter(visible = True)
        goods = Item.objects.filter(category= category_goods,visible = True)
    except:
        raise Http404('Объект не найден')
    context = {
        'goods' : goods,
        'category_item' : category_goods,
        'category' : category_menu,
        'username': auth.get_user(request).username,
        'cart': cart
    }
    return render_to_response('goods.html', context)
    


