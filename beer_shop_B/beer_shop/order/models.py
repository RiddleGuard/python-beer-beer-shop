from django.db import models
from main.models import Item
# Create your models here.
    
class order(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)
    email = models.EmailField(verbose_name='Email')
    index = models.CharField(max_length=255, verbose_name='Индекс')
    city = models.CharField(verbose_name='Город', max_length=100)
    prim = models.CharField(max_length=255, verbose_name='Примечание', default=' ')
    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Обновлен', auto_now=True)
    paid = models.BooleanField(verbose_name='Оплачен', default=False)
    class Meta:
        ordering = ('-created', )
        verbose_name="Заказ"
        verbose_name_plural="Заказы"
        
    def __str__(self):
        return 'Заказ: {}'.format(self.id)

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.order_items.all())
    
class orderItem(models.Model):
    goods_id = models.ForeignKey(Item , related_name='items')
    order_id = models.ForeignKey(order,on_delete=models.CASCADE, related_name='order_items')
    price = models.DecimalField(verbose_name='Цена', max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(verbose_name='Количество', default=1)

    class Meta:
        verbose_name="Перечень заказанного товара"
        verbose_name_plural="Перечени заказанных товаров"

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return self.price * self.quantity