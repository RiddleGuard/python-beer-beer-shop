from django.shortcuts import render, get_object_or_404
from order.models import orderItem, order
from order.forms import OrderCreateForm
from cart.cart import Cart
from django.contrib.admin.views.decorators import staff_member_required


@staff_member_required
def AdminOrderDetail(request, order_id):
    order1 = get_object_or_404(order, id=order_id)
    return render(request, 'detail1.html', {'order': order1})

def OrderCreate(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            order = form.save()
            for item in cart:
                orderItem.objects.create(order_id=order, goods_id=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            cart.clear()
            return render(request, 'created.html', {'cart': cart,'order': order})

    form = OrderCreateForm()
    return render(request, 'create.html', {'cart': cart,
                                                        'form': form})